![Panda Logo](./assets/logo_panda.png)

A simple statistics bot for Telegram groups, which is mainly intended to make writing in groups more fun

[![Pipeline Status][pipeline-badge]][pipeline-url]
[![Pylint][pylint-badge]][pylint-url]
[![coverage report][cover-badge]][cover-url]
[![Codestyle: black][codestyle-shield]][codestyle-url]
[![python version][python-version-shield]][python-version-url]
[![Python-Telegram-Bot][ptb-shield]][ptb-url]
[![telegram chat][telegram-chat-shield]][telegram-chat-url]

# Table of Contents
* [About Panda Statistics](#about)
  - [Build with](#buildwith)
* [Getting Started](#start)
  - [Installation](#installation)
  - [Configuration](#config)
  - [Running the bot](#running)
* [Usage](#usage)
* [Contributing](#contrib)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)

<a name="about"></a>
## About Panda Statistics
Panda Statistics is a simple Telegram bot written in Python that counts
messages in groups and outputs simple statistics with a high score. It is easy
to configure and easy to use.

![chat](./assets/screenshots/chat.png)

<a name="buildwith"></a>
### Build with
Panda Statistics uses [Python-Telegram-Bot][ptb-url], *'a wrapper you can't refuse'*.

<a name="start"></a>
## Getting Started
To get the bot up and running follow these simple steps.

<a name="installation"></a>
### Installation
First clone the repository:
```sh
git clone https://gitlab.com/chgaida/panda-statistics.git
```
Create and activate a virtual environment (recommended):
```sh
cd panda-statistics
python3 -m venv env
source env/bin/activate
```
Install with pip:
```sh
pip install -e .
```
Build the database (requires sqlite3 to be installed on your system):
```sh
./create_database.sh
```

<a name="config"></a>
### Configuration
Make a copy of `config.sample.yaml` and fill in the empty fields:
```
cp config.sample.yaml config.yaml
```
![config.yaml](./assets/screenshots/config.png)

You need to add at least your Telegram bot token and one or more groups (group
IDs) to the config file for the bot to run.

The output language for bot messages can be defined by setting the BOT_LANGUAGE option. Two languages ("en" - english, "de" - deutsch) are provided, english is set as default. Additional languages can be added by copying ./lang/en.yml into ./lang/{two-letter-locale-id}.yml and translating the english strings into the corresponding target language.

<a name="running"></a>
### Running the bot
To start the bot in polling mode type:
```sh
./panda_stat.py
```
If you want to use webhooks, use the following command line option:
```sh
./panda_stat.py webhooks
```
In this case you have to specify your server IP and the paths to your server
certificate and private key in the configuration file.

![config_webhook](./assets/screenshots/config_webhook.png)

<a name="usage"></a>
## Usage
There are only a few simple commands:

### In groups
* `/me` - shows your messages
* `/stat` - shows the statistics of the group from which the command was called
* `/networkstats` - shows the statistics of all groups the bot is in
* `/gid` - displays the group ID
* `/help` - shows a short help text

### In private chat with the bot
* `/me` - shows the number of your messages from all groups the bot and you are in.

<a name="contrib"></a>
## Contributing
Please take a look at [CONTRIBUTING.md](./CONTRIBUTING.md) if you're interested in helping!

<a name="license"></a>
## License
Distributed under the MIT License. See [LICENSE](./LICENSE) for more information.

<a name="contact"></a>
## Contact
Christian Gaida [@justplants_dev](https://t.me/justplants_dev) - Telegram 

Project Link: []()

<a name="acknowledgements"></a>
## Acknowledgements

* @thisdudeisvegan - for the idea
* Panda logo vector made by Larikoze from [pngtree](https://pngtree.com)




<!-- MARKDOWN LINKS & IMAGES -->
[pipeline-badge]: https://gitlab.com/chgaida/panda-statistics/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/chgaida/panda-statistics/-/commits/master
[pylint-badge]: https://gitlab.com/chgaida/panda-statistics/-/jobs/artifacts/master/raw/public/badges/pylint.svg?job=pylint
[pylint-url]: https://gitlab.com/chgaida/panda-statistics/-/commits/master
[cover-badge]: https://gitlab.com/chgaida/panda-statistics/badges/master/coverage.svg
[cover-url]: https://gitlab.com/chgaida/panda-statistics/-/commits/master
[codestyle-shield]: https://img.shields.io/badge/code%20style-black-000000.svg
[codestyle-url]: https://github.com/psf/black
[python-version-shield]: https://img.shields.io/badge/python-3.7-blue
[python-version-url]: https://www.python.org/downloads/release/python-370/
[ptb-shield]: https://img.shields.io/badge/Python--Telegram--Bot-v13.1-blue
[ptb-url]: https://python-telegram-bot.org/
[telegram-chat-shield]: https://img.shields.io/badge/Telegram-Group-informational
[telegram-chat-url]: https://t.me/justplants_bot_support
