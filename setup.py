#!/usr/bin/env python
"""The setup and build script for panda-statistics."""


import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()


def requirements():
    """Build the requirements list for this project"""
    requirements_list = []

    with open("requirements.txt") as requirements:
        for install in requirements:
            requirements_list.append(install.strip())

    return requirements_list


requirements = requirements()

setuptools.setup(
    name="panda-statistics-cri5h",  # Replace with your own username
    version="0.0.1",
    author="Christian Gaida",
    author_email="ch.gaida@mailbox.org",
    description="A simple Telegram statistics bot",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/chgaida/panda-statistics",
    packages=setuptools.find_packages(),
    install_requires=requirements,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
    python_requires=">=3.7",
)
