"""Test for the dbqueries.py module."""
# pylint: disable=W0613


import datetime
from sqlite3 import Error as DB_Error
import pytest
from panda import dbhelper
from panda import dbqueries


DATE = datetime.datetime.now()


def test_connection(init_db, mock_test_database):
    """Testing the connection with a patched SQLITE_DB path."""
    db = dbhelper.DBHelper()  # pylint: disable=C0103
    dbqueries.db_add_user("1hash", "test_cri5h_1")
    assert isinstance(db, dbhelper.DBHelper)


def test_add_user(mock_test_database):
    """Testing the db_add_user function."""
    dbqueries.db_add_user("2hash", "test_cri5h_2")
    stmt = "SELECT id FROM Telegram_User WHERE user_name='test_cri5h_2'"
    cursor = dbhelper.DBHelper().db.execute(stmt)
    result = cursor.fetchone()
    assert result[0] == 2


def test_add_group(mock_test_database):
    """Testing the db_add_group function."""
    dbqueries.db_add_group(1, "panda group")
    stmt = "SELECT group_id FROM Telegram_Group WHERE group_name='panda group'"
    cursor = dbhelper.DBHelper().db.execute(stmt)
    result = cursor.fetchone()
    group_id = result[0]
    expected = 1
    assert group_id == expected


def test_add_group_without_group_name(mock_test_database):
    """Testing the db_add_group function without group name."""
    dbqueries.db_add_group(2)
    stmt = "SELECT id FROM Telegram_Group WHERE group_id=2"
    cursor = dbhelper.DBHelper().db.execute(stmt)
    result = cursor.fetchone()
    group_id = result[0]
    expected = 2
    assert group_id == expected


def test_add_message(mock_test_database):
    """Testing the db_add_message function."""
    dbqueries.db_add_message(1, "1hash", "text", 2, DATE)
    stmt = "SELECT user_id FROM Message WHERE group_id=1"
    cursor = dbhelper.DBHelper().db.execute(stmt)
    result = cursor.fetchone()
    user_id = result[0]
    expected = 1
    assert user_id == expected


def test_add_message_2(mock_test_database):
    """Add a second message."""
    dbqueries.db_add_message(2, "1hash", "text", 2, DATE)
    stmt = "SELECT user_id FROM Message WHERE group_id=1"
    cursor = dbhelper.DBHelper().db.execute(stmt)
    result = cursor.fetchone()
    user_id = result[0]
    expected = 1
    assert user_id == expected


def test_add_message_with_non_existent_user(mock_test_database):
    """Test db_add_message with non existent user."""
    with pytest.raises(ValueError):
        dbqueries.db_add_message(1, "3hash", "text", 2, DATE)


def test_get_all_messages_with_group_id(mock_test_database):
    """Test db_get_all_messages from one group."""
    result = dbqueries.db_get_all_messages(1)
    expected = 1
    assert result == expected


def test_get_all_message_without_group_id(mock_test_database):
    """Test db_get_all_messages without arguments (should return _all_
    messages)."""
    result = dbqueries.db_get_all_messages()
    expected = 2
    assert result == expected


def test_get_all_message_return_type(mock_test_database):
    """Test for the correct return type."""
    result = dbqueries.db_get_all_messages()
    assert isinstance(result, int)


def test_get_all_messages_with_no_tables_in_db(mock_fail_database):
    """Testing get_all_messages  with no tables in the database."""
    with pytest.raises(DB_Error):
        dbqueries.db_get_all_messages()


def test_get_all_messages_with_non_valid_timespan(mock_test_database):
    """Testing with a timespan out of range."""
    with pytest.raises(ValueError):
        dbqueries.db_get_all_messages(1, 4)


def test_get_user_messages_with_group_id(mock_test_database):
    """Testing db_get_user_messages with group_id."""
    result = dbqueries.db_get_user_messages("1hash", 1)
    expected = 1
    assert result == expected


def test_get_user_messages_without_group_id(mock_test_database):
    """Testing db_get_user_messages without group_id (should return _all_
    messages.)"""
    result = dbqueries.db_get_user_messages("1hash")
    expected = 2
    assert result == expected


def test_get_user_message_return_type(mock_test_database):
    """Test for the correct return type."""
    result = dbqueries.db_get_user_messages("1hash")
    assert isinstance(result, int)


def test_get_user_messages_with_no_tables_in_db(mock_fail_database):
    """Testing get_user_messages with no tables in the database."""
    with pytest.raises(DB_Error):
        dbqueries.db_get_user_messages("1hash")


def test_get_message_types_with_group_id(mock_test_database):
    """Testing db_get_message_types with group id."""
    messages, msg_type = dbqueries.db_get_message_types(1)
    expected_total = 1
    expected_type = [(1, "text")]
    assert messages == expected_total
    assert msg_type == expected_type


def test_get_message_types_without_group_id(mock_test_database):
    """Testing db_get_message_types with no parameter. Should return
    total message and number of messages with types from _all_ groups."""
    messages, msg_type = dbqueries.db_get_message_types()
    expected_total = 2
    expected_type = [(2, "text")]
    assert messages == expected_total
    assert msg_type == expected_type


def test_get_message_types_with_non_existent_group_id(mock_test_database):
    """Testing with non existent group_id."""
    messages, msg_type = dbqueries.db_get_message_types(3)
    expected_total = 0
    expected_type = []
    assert messages == expected_total
    assert msg_type == expected_type


def test_get_message_types_return_types(mock_test_database):
    """Test for the return types."""
    message, msg_type = dbqueries.db_get_message_types(1)
    assert isinstance(message, int)
    assert isinstance(msg_type, list)


def test_get_message_types_with_non_valid_timespan(mock_test_database):
    """Testing with a timespan out of range."""
    with pytest.raises(ValueError):
        dbqueries.db_get_message_types(1, 4)


def test_get_message_types_with_no_tables_in_db(mock_fail_database):
    """Testing with non existent database tables."""
    with pytest.raises(DB_Error):
        dbqueries.db_get_message_types()
