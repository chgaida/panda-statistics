"""Setup test suite."""


import os
import datetime
import sqlite3
from contextlib import closing
import pytest

from telegram import Update, Message, User, Chat
from panda import dbhelper


DATE = datetime.datetime.now()


@pytest.fixture(scope="module")
def init_db():
    """Setup test database."""
    with closing(connect_db()) as db:  # pylint: disable=C0103
        with open("panda/db/schema.sql") as fp:  # pylint: disable=C0103
            db.cursor().executescript(fp.read())  # pylint: disable=E1101
            db.commit()  # pylint: disable=E1101
        yield
        if os.path.exists("tests/test_db.sqlite3"):
            os.remove("tests/test_db.sqlite3")


def connect_db():
    """Return a sqlite connection for the test database."""
    return sqlite3.connect("tests/test_db.sqlite3")  # :memory:")


@pytest.fixture
def mock_test_database(monkeypatch):
    """Set the database path to :memory:"""
    monkeypatch.setattr(
        dbhelper.DBHelper, "dbpath", "tests/test_db.sqlite3"
    )  # ":memory:")


@pytest.fixture
def mock_fail_database(monkeypatch):
    """Set the database path to :memory:"""
    monkeypatch.setattr(dbhelper.DBHelper, "dbpath", ":memory:")


def create_user(**kwargs):
    """Create a Telegram User object."""
    return User(
        id=1,
        is_bot=False,
        first_name=kwargs.pop("first_name", "Christian"),
        username=kwargs.pop("username", "cri5h"),
        language_code="en",
        **kwargs
    )


def create_message(text, **kwargs):
    """Create a Telegram message object."""
    user = create_user(**kwargs.pop("user", User(id=1, first_name="", is_bot=False)))
    return Message(
        message_id=1,
        from_user=user,
        date=DATE,
        chat=Chat(id=1, type="private"),
        text=text,
        **kwargs
    )


def create_update(message, message_factory=create_message, **kwargs):
    """Create a Telegram Update object."""
    if not isinstance(message, Message):
        message = message_factory(message, **kwargs)
    return Update(0, message)


@pytest.fixture
def update():
    """Provide an Update object."""
    return create_update("Hello World!", user={"first_name": "Christian"})
